package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.StorageEndpoint;

@Component
public class ServiceHostCommand extends AbstractCommand {

    @Autowired
    private StorageEndpoint storageEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.SERVER_HOST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show server host";
    }

    @Override
    public void execute() {
        System.out.println("[HOST]");
        System.out.println(storageEndpoint.getServerHostInfo());
        System.out.println("[OK]");
    }

}
