package ru.ovechkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectEndpoint;
import ru.ovechkin.tm.endpoint.TaskDTO;
import ru.ovechkin.tm.endpoint.TaskEndpoint;

import java.util.List;

@Component
public final class TaskListCommand extends AbstractCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_TASK_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        @NotNull final List<TaskDTO> tasksDTO = taskEndpoint.findUserTasks(sessionDTO);
        @NotNull int index = 1;
        for (@NotNull final TaskDTO taskDTO : tasksDTO) {
            System.out.println(index + ". " + taskDTO.getName() + ". PROJECT: " + taskDTO.getProjectId());
            index++;
        }
        System.out.println("[OK]");
    }

}