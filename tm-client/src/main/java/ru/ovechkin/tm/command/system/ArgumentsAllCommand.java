package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.config.ClientConfiguration;
import ru.ovechkin.tm.bootstrap.Bootstrap;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

import java.util.Collection;

@Component
public final class ArgumentsAllCommand extends AbstractCommand {

    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_ARGUMENTS;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_ARGUMENTS;
    }

    @NotNull
    @Override
    public String description() {
        return "Show available arguments";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Collection<AbstractCommand> arguments = bootstrap.getArguments();
        for (@NotNull final AbstractCommand argument : arguments) System.out.println(argument.arg());
    }

}