package ru.ovechkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.TaskDTO;
import ru.ovechkin.tm.endpoint.TaskEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdCommand extends AbstractCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.TASK_SHOW_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER TASK ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskDTO taskDTO = taskEndpoint.findTaskById(sessionDTO, id);
        System.out.println("ID: " + taskDTO.getId());
        System.out.println("NAME: " + taskDTO.getName());
        System.out.println("DESCRIPTION: " + taskDTO.getDescription());
        System.out.println("[COMPLETE]");
    }

}