package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

@Component
public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_VERSION;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_VERSION;
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.9.0");
    }

}
