package ru.ovechkin.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.StorageEndpoint;

@Component
public class DataBase64SaveCommand extends AbstractCommand {

    @Autowired
    private StorageEndpoint storageEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_BASE64_SAVE;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        storageEndpoint.dataBase64Save(sessionDTO);
        System.out.println("[OK]");
    }

}