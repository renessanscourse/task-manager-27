package ru.ovechkin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.endpoint.SessionDTO;
import ru.ovechkin.tm.enumirated.Role;

@Component
public abstract class AbstractCommand {

    @NotNull
    protected final static SessionDTO sessionDTO = new SessionDTO();

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute() throws Exception;

    public AbstractCommand() {
    }

}