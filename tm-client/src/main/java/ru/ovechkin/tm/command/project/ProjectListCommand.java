package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectDTO;
import ru.ovechkin.tm.endpoint.ProjectEndpoint;

import java.util.List;

@Component
public final class ProjectListCommand extends AbstractCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @NotNull final List<ProjectDTO> projectsDTO = projectEndpoint.findUserProjects(sessionDTO);
        int index = 1;
        for (final ProjectDTO projectDTO : projectsDTO) {
            System.out.println(index + ". " + projectDTO.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}