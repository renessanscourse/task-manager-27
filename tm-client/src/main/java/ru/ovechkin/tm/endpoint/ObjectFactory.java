
package ru.ovechkin.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.ovechkin.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClassNotFoundException_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "ClassNotFoundException");
    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "Exception");
    private final static QName _IOException_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "IOException");
    private final static QName _JAXBException_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "JAXBException");
    private final static QName _DataBase64Load_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataBase64Load");
    private final static QName _DataBase64LoadResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataBase64LoadResponse");
    private final static QName _DataBase64Save_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataBase64Save");
    private final static QName _DataBase64SaveResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataBase64SaveResponse");
    private final static QName _DataBinaryLoad_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataBinaryLoad");
    private final static QName _DataBinaryLoadResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataBinaryLoadResponse");
    private final static QName _DataBinarySave_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataBinarySave");
    private final static QName _DataBinarySaveResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataBinarySaveResponse");
    private final static QName _DataJsonJaxbLoad_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataJsonJaxbLoad");
    private final static QName _DataJsonJaxbLoadResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataJsonJaxbLoadResponse");
    private final static QName _DataJsonJaxbSave_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataJsonJaxbSave");
    private final static QName _DataJsonJaxbSaveResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataJsonJaxbSaveResponse");
    private final static QName _DataJsonMapperLoad_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataJsonMapperLoad");
    private final static QName _DataJsonMapperLoadResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataJsonMapperLoadResponse");
    private final static QName _DataJsonMapperSave_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataJsonMapperSave");
    private final static QName _DataJsonMapperSaveResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataJsonMapperSaveResponse");
    private final static QName _DataXmlJaxbLoad_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataXmlJaxbLoad");
    private final static QName _DataXmlJaxbLoadResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataXmlJaxbLoadResponse");
    private final static QName _DataXmlJaxbSave_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataXmlJaxbSave");
    private final static QName _DataXmlJaxbSaveResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataXmlJaxbSaveResponse");
    private final static QName _DataXmlMapperLoad_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataXmlMapperLoad");
    private final static QName _DataXmlMapperLoadResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataXmlMapperLoadResponse");
    private final static QName _DataXmlMapperSave_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataXmlMapperSave");
    private final static QName _DataXmlMapperSaveResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "dataXmlMapperSaveResponse");
    private final static QName _GetServerHostInfo_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "getServerHostInfo");
    private final static QName _GetServerHostInfoResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "getServerHostInfoResponse");
    private final static QName _GetServerPortInfo_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "getServerPortInfo");
    private final static QName _GetServerPortInfoResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "getServerPortInfoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.ovechkin.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClassNotFoundException }
     * 
     */
    public ClassNotFoundException createClassNotFoundException() {
        return new ClassNotFoundException();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link IOException }
     * 
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link JAXBException }
     * 
     */
    public JAXBException createJAXBException() {
        return new JAXBException();
    }

    /**
     * Create an instance of {@link DataBase64Load }
     * 
     */
    public DataBase64Load createDataBase64Load() {
        return new DataBase64Load();
    }

    /**
     * Create an instance of {@link DataBase64LoadResponse }
     * 
     */
    public DataBase64LoadResponse createDataBase64LoadResponse() {
        return new DataBase64LoadResponse();
    }

    /**
     * Create an instance of {@link DataBase64Save }
     * 
     */
    public DataBase64Save createDataBase64Save() {
        return new DataBase64Save();
    }

    /**
     * Create an instance of {@link DataBase64SaveResponse }
     * 
     */
    public DataBase64SaveResponse createDataBase64SaveResponse() {
        return new DataBase64SaveResponse();
    }

    /**
     * Create an instance of {@link DataBinaryLoad }
     * 
     */
    public DataBinaryLoad createDataBinaryLoad() {
        return new DataBinaryLoad();
    }

    /**
     * Create an instance of {@link DataBinaryLoadResponse }
     * 
     */
    public DataBinaryLoadResponse createDataBinaryLoadResponse() {
        return new DataBinaryLoadResponse();
    }

    /**
     * Create an instance of {@link DataBinarySave }
     * 
     */
    public DataBinarySave createDataBinarySave() {
        return new DataBinarySave();
    }

    /**
     * Create an instance of {@link DataBinarySaveResponse }
     * 
     */
    public DataBinarySaveResponse createDataBinarySaveResponse() {
        return new DataBinarySaveResponse();
    }

    /**
     * Create an instance of {@link DataJsonJaxbLoad }
     * 
     */
    public DataJsonJaxbLoad createDataJsonJaxbLoad() {
        return new DataJsonJaxbLoad();
    }

    /**
     * Create an instance of {@link DataJsonJaxbLoadResponse }
     * 
     */
    public DataJsonJaxbLoadResponse createDataJsonJaxbLoadResponse() {
        return new DataJsonJaxbLoadResponse();
    }

    /**
     * Create an instance of {@link DataJsonJaxbSave }
     * 
     */
    public DataJsonJaxbSave createDataJsonJaxbSave() {
        return new DataJsonJaxbSave();
    }

    /**
     * Create an instance of {@link DataJsonJaxbSaveResponse }
     * 
     */
    public DataJsonJaxbSaveResponse createDataJsonJaxbSaveResponse() {
        return new DataJsonJaxbSaveResponse();
    }

    /**
     * Create an instance of {@link DataJsonMapperLoad }
     * 
     */
    public DataJsonMapperLoad createDataJsonMapperLoad() {
        return new DataJsonMapperLoad();
    }

    /**
     * Create an instance of {@link DataJsonMapperLoadResponse }
     * 
     */
    public DataJsonMapperLoadResponse createDataJsonMapperLoadResponse() {
        return new DataJsonMapperLoadResponse();
    }

    /**
     * Create an instance of {@link DataJsonMapperSave }
     * 
     */
    public DataJsonMapperSave createDataJsonMapperSave() {
        return new DataJsonMapperSave();
    }

    /**
     * Create an instance of {@link DataJsonMapperSaveResponse }
     * 
     */
    public DataJsonMapperSaveResponse createDataJsonMapperSaveResponse() {
        return new DataJsonMapperSaveResponse();
    }

    /**
     * Create an instance of {@link DataXmlJaxbLoad }
     * 
     */
    public DataXmlJaxbLoad createDataXmlJaxbLoad() {
        return new DataXmlJaxbLoad();
    }

    /**
     * Create an instance of {@link DataXmlJaxbLoadResponse }
     * 
     */
    public DataXmlJaxbLoadResponse createDataXmlJaxbLoadResponse() {
        return new DataXmlJaxbLoadResponse();
    }

    /**
     * Create an instance of {@link DataXmlJaxbSave }
     * 
     */
    public DataXmlJaxbSave createDataXmlJaxbSave() {
        return new DataXmlJaxbSave();
    }

    /**
     * Create an instance of {@link DataXmlJaxbSaveResponse }
     * 
     */
    public DataXmlJaxbSaveResponse createDataXmlJaxbSaveResponse() {
        return new DataXmlJaxbSaveResponse();
    }

    /**
     * Create an instance of {@link DataXmlMapperLoad }
     * 
     */
    public DataXmlMapperLoad createDataXmlMapperLoad() {
        return new DataXmlMapperLoad();
    }

    /**
     * Create an instance of {@link DataXmlMapperLoadResponse }
     * 
     */
    public DataXmlMapperLoadResponse createDataXmlMapperLoadResponse() {
        return new DataXmlMapperLoadResponse();
    }

    /**
     * Create an instance of {@link DataXmlMapperSave }
     * 
     */
    public DataXmlMapperSave createDataXmlMapperSave() {
        return new DataXmlMapperSave();
    }

    /**
     * Create an instance of {@link DataXmlMapperSaveResponse }
     * 
     */
    public DataXmlMapperSaveResponse createDataXmlMapperSaveResponse() {
        return new DataXmlMapperSaveResponse();
    }

    /**
     * Create an instance of {@link GetServerHostInfo }
     * 
     */
    public GetServerHostInfo createGetServerHostInfo() {
        return new GetServerHostInfo();
    }

    /**
     * Create an instance of {@link GetServerHostInfoResponse }
     * 
     */
    public GetServerHostInfoResponse createGetServerHostInfoResponse() {
        return new GetServerHostInfoResponse();
    }

    /**
     * Create an instance of {@link GetServerPortInfo }
     * 
     */
    public GetServerPortInfo createGetServerPortInfo() {
        return new GetServerPortInfo();
    }

    /**
     * Create an instance of {@link GetServerPortInfoResponse }
     * 
     */
    public GetServerPortInfoResponse createGetServerPortInfoResponse() {
        return new GetServerPortInfoResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link Throwable }
     * 
     */
    public Throwable createThrowable() {
        return new Throwable();
    }

    /**
     * Create an instance of {@link StackTraceElement }
     * 
     */
    public StackTraceElement createStackTraceElement() {
        return new StackTraceElement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClassNotFoundException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "ClassNotFoundException")
    public JAXBElement<ClassNotFoundException> createClassNotFoundException(ClassNotFoundException value) {
        return new JAXBElement<ClassNotFoundException>(_ClassNotFoundException_QNAME, ClassNotFoundException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JAXBException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "JAXBException")
    public JAXBElement<JAXBException> createJAXBException(JAXBException value) {
        return new JAXBElement<JAXBException>(_JAXBException_QNAME, JAXBException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64Load }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataBase64Load")
    public JAXBElement<DataBase64Load> createDataBase64Load(DataBase64Load value) {
        return new JAXBElement<DataBase64Load>(_DataBase64Load_QNAME, DataBase64Load.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64LoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataBase64LoadResponse")
    public JAXBElement<DataBase64LoadResponse> createDataBase64LoadResponse(DataBase64LoadResponse value) {
        return new JAXBElement<DataBase64LoadResponse>(_DataBase64LoadResponse_QNAME, DataBase64LoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64Save }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataBase64Save")
    public JAXBElement<DataBase64Save> createDataBase64Save(DataBase64Save value) {
        return new JAXBElement<DataBase64Save>(_DataBase64Save_QNAME, DataBase64Save.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64SaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataBase64SaveResponse")
    public JAXBElement<DataBase64SaveResponse> createDataBase64SaveResponse(DataBase64SaveResponse value) {
        return new JAXBElement<DataBase64SaveResponse>(_DataBase64SaveResponse_QNAME, DataBase64SaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinaryLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataBinaryLoad")
    public JAXBElement<DataBinaryLoad> createDataBinaryLoad(DataBinaryLoad value) {
        return new JAXBElement<DataBinaryLoad>(_DataBinaryLoad_QNAME, DataBinaryLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinaryLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataBinaryLoadResponse")
    public JAXBElement<DataBinaryLoadResponse> createDataBinaryLoadResponse(DataBinaryLoadResponse value) {
        return new JAXBElement<DataBinaryLoadResponse>(_DataBinaryLoadResponse_QNAME, DataBinaryLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinarySave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataBinarySave")
    public JAXBElement<DataBinarySave> createDataBinarySave(DataBinarySave value) {
        return new JAXBElement<DataBinarySave>(_DataBinarySave_QNAME, DataBinarySave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinarySaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataBinarySaveResponse")
    public JAXBElement<DataBinarySaveResponse> createDataBinarySaveResponse(DataBinarySaveResponse value) {
        return new JAXBElement<DataBinarySaveResponse>(_DataBinarySaveResponse_QNAME, DataBinarySaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonJaxbLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataJsonJaxbLoad")
    public JAXBElement<DataJsonJaxbLoad> createDataJsonJaxbLoad(DataJsonJaxbLoad value) {
        return new JAXBElement<DataJsonJaxbLoad>(_DataJsonJaxbLoad_QNAME, DataJsonJaxbLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonJaxbLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataJsonJaxbLoadResponse")
    public JAXBElement<DataJsonJaxbLoadResponse> createDataJsonJaxbLoadResponse(DataJsonJaxbLoadResponse value) {
        return new JAXBElement<DataJsonJaxbLoadResponse>(_DataJsonJaxbLoadResponse_QNAME, DataJsonJaxbLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonJaxbSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataJsonJaxbSave")
    public JAXBElement<DataJsonJaxbSave> createDataJsonJaxbSave(DataJsonJaxbSave value) {
        return new JAXBElement<DataJsonJaxbSave>(_DataJsonJaxbSave_QNAME, DataJsonJaxbSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonJaxbSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataJsonJaxbSaveResponse")
    public JAXBElement<DataJsonJaxbSaveResponse> createDataJsonJaxbSaveResponse(DataJsonJaxbSaveResponse value) {
        return new JAXBElement<DataJsonJaxbSaveResponse>(_DataJsonJaxbSaveResponse_QNAME, DataJsonJaxbSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonMapperLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataJsonMapperLoad")
    public JAXBElement<DataJsonMapperLoad> createDataJsonMapperLoad(DataJsonMapperLoad value) {
        return new JAXBElement<DataJsonMapperLoad>(_DataJsonMapperLoad_QNAME, DataJsonMapperLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonMapperLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataJsonMapperLoadResponse")
    public JAXBElement<DataJsonMapperLoadResponse> createDataJsonMapperLoadResponse(DataJsonMapperLoadResponse value) {
        return new JAXBElement<DataJsonMapperLoadResponse>(_DataJsonMapperLoadResponse_QNAME, DataJsonMapperLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonMapperSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataJsonMapperSave")
    public JAXBElement<DataJsonMapperSave> createDataJsonMapperSave(DataJsonMapperSave value) {
        return new JAXBElement<DataJsonMapperSave>(_DataJsonMapperSave_QNAME, DataJsonMapperSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonMapperSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataJsonMapperSaveResponse")
    public JAXBElement<DataJsonMapperSaveResponse> createDataJsonMapperSaveResponse(DataJsonMapperSaveResponse value) {
        return new JAXBElement<DataJsonMapperSaveResponse>(_DataJsonMapperSaveResponse_QNAME, DataJsonMapperSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlJaxbLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataXmlJaxbLoad")
    public JAXBElement<DataXmlJaxbLoad> createDataXmlJaxbLoad(DataXmlJaxbLoad value) {
        return new JAXBElement<DataXmlJaxbLoad>(_DataXmlJaxbLoad_QNAME, DataXmlJaxbLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlJaxbLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataXmlJaxbLoadResponse")
    public JAXBElement<DataXmlJaxbLoadResponse> createDataXmlJaxbLoadResponse(DataXmlJaxbLoadResponse value) {
        return new JAXBElement<DataXmlJaxbLoadResponse>(_DataXmlJaxbLoadResponse_QNAME, DataXmlJaxbLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlJaxbSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataXmlJaxbSave")
    public JAXBElement<DataXmlJaxbSave> createDataXmlJaxbSave(DataXmlJaxbSave value) {
        return new JAXBElement<DataXmlJaxbSave>(_DataXmlJaxbSave_QNAME, DataXmlJaxbSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlJaxbSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataXmlJaxbSaveResponse")
    public JAXBElement<DataXmlJaxbSaveResponse> createDataXmlJaxbSaveResponse(DataXmlJaxbSaveResponse value) {
        return new JAXBElement<DataXmlJaxbSaveResponse>(_DataXmlJaxbSaveResponse_QNAME, DataXmlJaxbSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlMapperLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataXmlMapperLoad")
    public JAXBElement<DataXmlMapperLoad> createDataXmlMapperLoad(DataXmlMapperLoad value) {
        return new JAXBElement<DataXmlMapperLoad>(_DataXmlMapperLoad_QNAME, DataXmlMapperLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlMapperLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataXmlMapperLoadResponse")
    public JAXBElement<DataXmlMapperLoadResponse> createDataXmlMapperLoadResponse(DataXmlMapperLoadResponse value) {
        return new JAXBElement<DataXmlMapperLoadResponse>(_DataXmlMapperLoadResponse_QNAME, DataXmlMapperLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlMapperSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataXmlMapperSave")
    public JAXBElement<DataXmlMapperSave> createDataXmlMapperSave(DataXmlMapperSave value) {
        return new JAXBElement<DataXmlMapperSave>(_DataXmlMapperSave_QNAME, DataXmlMapperSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlMapperSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "dataXmlMapperSaveResponse")
    public JAXBElement<DataXmlMapperSaveResponse> createDataXmlMapperSaveResponse(DataXmlMapperSaveResponse value) {
        return new JAXBElement<DataXmlMapperSaveResponse>(_DataXmlMapperSaveResponse_QNAME, DataXmlMapperSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServerHostInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "getServerHostInfo")
    public JAXBElement<GetServerHostInfo> createGetServerHostInfo(GetServerHostInfo value) {
        return new JAXBElement<GetServerHostInfo>(_GetServerHostInfo_QNAME, GetServerHostInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServerHostInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "getServerHostInfoResponse")
    public JAXBElement<GetServerHostInfoResponse> createGetServerHostInfoResponse(GetServerHostInfoResponse value) {
        return new JAXBElement<GetServerHostInfoResponse>(_GetServerHostInfoResponse_QNAME, GetServerHostInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServerPortInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "getServerPortInfo")
    public JAXBElement<GetServerPortInfo> createGetServerPortInfo(GetServerPortInfo value) {
        return new JAXBElement<GetServerPortInfo>(_GetServerPortInfo_QNAME, GetServerPortInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServerPortInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "getServerPortInfoResponse")
    public JAXBElement<GetServerPortInfoResponse> createGetServerPortInfoResponse(GetServerPortInfoResponse value) {
        return new JAXBElement<GetServerPortInfoResponse>(_GetServerPortInfoResponse_QNAME, GetServerPortInfoResponse.class, null, value);
    }

}
