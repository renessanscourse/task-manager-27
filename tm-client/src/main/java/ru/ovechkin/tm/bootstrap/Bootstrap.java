package ru.ovechkin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.exeption.empty.CommandEmptyException;
import ru.ovechkin.tm.exeption.other.WrongCommandException;
import ru.ovechkin.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class Bootstrap {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    public void run(@Nullable final String[] args) throws Exception{
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        process();
    }

    @Autowired
    private void initCommandAndArgumentMap(@NotNull final AbstractCommand[] abstractCommands) {
        if (abstractCommands == null) return;
        for (AbstractCommand command : abstractCommands) {
            commands.put(command.name(), command);
            arguments.put(command.arg(), command);
        }
    }

    private boolean parseArgs(@Nullable final String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        @NotNull final String arg = args[0];
        parseArg(arg);
        parseCommand(arg);
        return true;
    }

    public void parseArg(@Nullable final String arg) throws Exception {
        if (arg.isEmpty()) return;
        @NotNull final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new WrongCommandException(arg);
        argument.execute();
    }

    private void process() {
        String cmd = "";
        while (!CmdConst.CMD_EXIT.equals(cmd)) {
            System.out.print("Enter command: ");
            cmd = TerminalUtil.nextLine();
            try {
                parseCommand(cmd);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                e.printStackTrace();
                System.err.println("[FAIL]");
            }
            System.out.println();
        }
    }

    private void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) throw new CommandEmptyException();
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new WrongCommandException(cmd);
        command.execute();
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

}