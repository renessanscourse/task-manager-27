package ru.ovechkin.tm.exeption.empty;

public class EmailEmptyException extends RuntimeException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}