package ru.ovechkin.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.config.ServerConfiguration;
import ru.ovechkin.tm.service.UserService;

import javax.persistence.EntityManager;
import java.util.Arrays;

public class ServerTest {

    @NotNull final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    private IUserService userService = context.getBean(UserService.class);

    @Test
    public void name() {
        System.out.println(Arrays.toString(context.getBean(ServerConfiguration.class).getClass().getFields()));
        System.out.println(userService.getEntityManager().hashCode());
        System.out.println(userService.getEntityManager().hashCode());
        System.out.println(userService.getEntityManager().hashCode());
        System.out.println();
        System.out.println(context.getBean(EntityManager.class));
        System.out.println(context.getBean(EntityManager.class));
        System.out.println(context.getBean(EntityManager.class));
    }
}