package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    User add(@NotNull User user);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @NotNull
    User removeUser(@NotNull User user);

    @Nullable
    User removeById(@NotNull String id);

    @Nullable
    User removeByLogin(@NotNull String login);

    @Nullable List<User> getAllUsers();

    @NotNull List<User> mergeCollection(@NotNull List<User> userList);

    void removeAllUsers();
}