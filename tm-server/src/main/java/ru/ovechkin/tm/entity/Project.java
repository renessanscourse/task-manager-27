package ru.ovechkin.tm.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.ProjectDTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "app_project")
public class Project extends AbstractEntity {

    @NotNull
    @ManyToOne
    private User user;

    @Nullable
    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    @Column(columnDefinition = "DATETIME", updatable = false)
    private Date creationTime = new Date(System.currentTimeMillis());

    public Project() {
    }

    @NotNull
    public User getUser() {
        return user;
    }

    public void setUser(@NotNull User user) {
        this.user = user;
    }

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable Date startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(@Nullable Date finishDate) {
        this.finishDate = finishDate;
    }

    @Nullable
    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(@Nullable Date creationTime) {
        this.creationTime = creationTime;
    }

    @Nullable
    public static Project toEntity(@Nullable ProjectDTO projectDTO) {
        if (projectDTO == null) return  null;
        return new Project(projectDTO);
    }

    public Project(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) return;
        setId(projectDTO.getId());
        setName(projectDTO.getName());
        setDescription(projectDTO.getDescription());
    }

    @Override
    public String toString() {
        return "Project{\n" +
                "user=" + user.getLogin() +
                "\n, name='" + name + '\'' +
                "\n, description='" + description + '\'' +
                "\n, tasks=" + tasks +
                '}';
    }
}