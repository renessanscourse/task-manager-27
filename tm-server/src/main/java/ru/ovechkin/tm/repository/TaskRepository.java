package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    @NotNull
    private EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final Task task) {
        entityManager.persist(task);
    }

    @Nullable
    @Override
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "FROM Task WHERE user_id = :userId AND id = :id", Task.class)
                .setMaxResults(1)
                .setParameter("userId", userId)
                .setParameter("id", id);
        if (query == null) return null;
        return query.getSingleResult();
    }

    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "FROM Task WHERE user_id = :userId AND name = :name", Task.class)
                .setMaxResults(1)
                .setParameter("userId", userId)
                .setParameter("name", name);
        if (query == null) return null;
        return query.getSingleResult();
    }

    @Nullable
    @Override
    public final List<Task> findUserTasks(@NotNull final String userId) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "FROM Task WHERE user_id = :userId", Task.class)
                .setParameter("userId", userId);
        if (query == null) return null;
        return query.getResultList();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE Task WHERE user_id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public Task removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        entityManager.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        entityManager.remove(task);
        return task;
    }


    @Nullable
    @Override
    public List<Task> getAllTasks() {
        @Nullable final TypedQuery<Task> query = entityManager.createQuery("FROM Task", Task.class);
        if (query == null) return null;
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Task> mergeCollection(@NotNull final List<Task> taskList) {
        for (@NotNull final Task task : taskList) {
            entityManager.persist(task);
        }
        return taskList;
    }

    @Override
    public void removeAllTasks() {
        entityManager.createQuery("DELETE FROM Task", Task.class)
                .executeUpdate();
    }

}