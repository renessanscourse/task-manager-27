package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.entity.Project;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    @NotNull
    private EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final Project project) {
        entityManager.merge(project);
    }

    @Nullable
    @Override
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TypedQuery<Project> query = entityManager.createQuery(
                "FROM Project WHERE user_id = :userId AND id = :id", Project.class)
                .setMaxResults(1)
                .setParameter("userId", userId)
                .setParameter("id", id);
        if (query == null) return null;
        return query.getSingleResult();
    }

    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "FROM Project WHERE user_id = :userId AND name = :name", Project.class)
                .setMaxResults(1)
                .setParameter("userId", userId)
                .setParameter("name", name);
        if (query == null) return null;
        return query.getSingleResult();
    }

    @Nullable
    @Override
    public final List<Project> findUserProjects(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "FROM Project WHERE user_id = :userId", Project.class)
                .setMaxResults(1)
                .setParameter("userId", userId);
        if (query == null) return null;
        return query.getResultList();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE Project WHERE user_id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }

    @Nullable
    @Override
    public List<Project> getAllProjects() {
        @Nullable final TypedQuery<Project> query = entityManager.createQuery("FROM Project", Project.class);
        if (query == null) return Collections.emptyList();
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Project> mergeCollection(@NotNull final List<Project> projectList) {
        for (@NotNull final Project project : projectList) {
            entityManager.merge(project);
        }
        return projectList;
    }

    @Override
    public void removeAllProjects() {
        entityManager.createQuery("DELETE FROM Project")
                .executeUpdate();
    }

}