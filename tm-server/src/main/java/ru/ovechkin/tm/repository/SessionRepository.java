package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.entity.Session;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) {
        entityManager.persist(session);
        return session;
    }

    @Nullable
    public Session findById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Nullable
    public List<Session> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        final TypedQuery<Session> query = entityManager.createQuery(
                "FROM Session WHERE user_id = :userId", Session.class)
                .setParameter("userId", userId);
        if (query == null) return null;
        return query.getResultList();
    }

    public void remove(@NotNull final Session session) {
        entityManager.remove(session);
    }

    public void removeByUserId(@Nullable final String userId) {
        entityManager.createQuery(
                "DELETE Session WHERE user_id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    public boolean contains(@NotNull final String id) {
        findById(id);
        return true;
    }

}