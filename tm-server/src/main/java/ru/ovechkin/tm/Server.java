package ru.ovechkin.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ovechkin.tm.bootstrap.Bootstrap;
import ru.ovechkin.tm.config.ServerConfiguration;

public class Server {

    public static void main(final String[] args) throws Exception {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ServerConfiguration.class);
        final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}